'''
2021 Participatory Image Archives
Contact: Adrian Demleitner, adrian.demleitner@unibas.ch

https://github.com/Participatory-Image-Archives/salsah-export
'''

import csv, json, sqlite3, datetime

from lxml import etree as et

import salsah_export_tools as sext

list_props = {}

'''with open('swiss_boundaries/swissBOUNDARIES3D_1_3_TLM_KANTONSGEBIET.geojson') as f:
    cantons = json.load(f)'''
with open('swiss_boundaries/swissBOUNDARIES3D_1_3_TLM_BEZIRKSGEBIET.geojson') as f:
    districts = json.load(f)
with open('swiss_boundaries/swissBOUNDARIES3D_1_3_TLM_HOHEITSGEBIET.geojson') as f:
    territories = json.load(f)

def main():
    # read project ontology and parse lists for easier access
    json_path = 'salsah-export/sgv.json'
    with open(json_path) as f:
        ontology = json.load(f)

    lists = sext.json_extract(ontology, 'nodes')

    for l in lists:
        for p in l:
            list_props[p['name']] = p['labels']['de']

    # db connection
    conn = sqlite3.connect("/Users/adriandemleitner/Repositories/pia-database/database/pia.sqlite")
    cur = conn.cursor()

    truncate(cur)
    populate_collections(cur)
    populate_agents(cur)
    populate_keywords(cur)
    populate_album(cur)
    populate_from_archive(cur, conn, 'SGV_10')
    populate_from_archive(cur, conn, 'SGV_12')
    
    conn.commit()
    cur.close()

'''
    Truncate: Empty that database
'''
def truncate(cur):
    print('Truncating Database')

    tables_query = "SELECT name FROM sqlite_schema WHERE type ='table' AND name NOT LIKE 'sqlite_%';"
    tables = cur.execute(tables_query)

    for table in tables:
        cur.execute("Delete from "+table[0]+";")

def populate_keywords(cur):
    print('Populating Keywords')

    parser = et.XMLParser(remove_blank_text=True)
    salsah_export = et.parse('salsah-export/Subject/sgv.xml', parser)
    root = salsah_export.getroot();

    for resource in root.iter('resource'):
        sql = 'Insert into "keywords" (salsah_id, label, description, origin, created_at) values (?, ?, ?, ?, ?) returning id;'
        values = [
            resource.get('id'), 
            resource.get('label'),
            "", "salsah",
            datetime.datetime.now()
        ]
        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))
            if property_type == 'Description_rt':
                values[2] = text_prop[0].text

        cur.execute(sql, tuple(values))
        keyword_id = cur.fetchone()[0]

        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))
            if property_type == 'name':
                for label in text_prop:
                    cur.execute('Insert into "alt_labels" (label, created_at) values (?, ?) returning id;', tuple([label.text, datetime.datetime.now()]))
                    alt_label_id = cur.fetchone()[0]
                    cur.execute('Insert into "alt_label_keyword" (keyword_id, alt_label_id) values (?, ?);', tuple([keyword_id, alt_label_id]))

def populate_agents(cur):
    print('Populating Agents')

    parser = et.XMLParser(remove_blank_text=True)
    salsah_export = et.parse('salsah-export/Persons/sgv.xml', parser)
    root = salsah_export.getroot();
    
    for resource in root.iter('resource'):
        sql = '''Insert into "agents" (salsah_id, name, description, family) 
                    values (?, ?, ?, ?) returning id;'''
        values = [
            resource.get('id'), 
            resource.get('label'),
            "", ""
        ]
        comment = []

        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))

            if property_type == 'description_rt':
                values[2] = sext.rmnl(text_prop[0].text)
            if property_type == 'family':
                values[3] = sext.rmnl(text_prop[0].text)
            if property_type == 'salsah_comment':
                for c in text_prop.iter('text'):
                    comment.append(c.text)

        cur.execute(sql, tuple(values))
        agent_id = cur.fetchone()[0]

        # add alt names/labels
        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))

            if property_type == 'name':
                for name in text_prop.iter('text'):
                    cur.execute('Insert into "alt_labels" (label, created_at) values (?, ?) returning id;', tuple([name.text, datetime.datetime.now()]))
                    alt_label_id = cur.fetchone()[0]
                    cur.execute('Insert into "agent_alt_label" (agent_id, alt_label_id) values (?, ?);', tuple([agent_id, alt_label_id]))
            
            if property_type == 'job':
                for job in text_prop.iter('text'):
                    cur.execute('Insert into "jobs" (label, created_at) values (?, ?) returning id;', tuple([job.text, datetime.datetime.now()]))
                    job_id = cur.fetchone()[0]
                    cur.execute('Insert into "agent_job" (agent_id, job_id) values (?, ?);', tuple([agent_id, job_id]))

            if property_type == 'literature':
                for literature in text_prop.iter('text'):
                    cur.execute('Insert into "literatures" (label, created_at) values (?, ?) returning id;', tuple([literature.text, datetime.datetime.now()]))
                    literature_id = cur.fetchone()[0]
                    cur.execute('Insert into "agent_literature" (agent_id, literature_id) values (?, ?);', tuple([agent_id, literature_id]))

        populate_comment(cur, comment, 'agent_comment', 'agent', agent_id)

        for date_prop in resource.iter('date-prop'):
            date_type = sext.fix_type_name(date_prop.get('name'))
            date_accuracy = get_date_accuracy(date_prop[0].text)
            if date_type == 'birthdate':
                cur.execute('Insert into "dates" (date, date_string, accuracy, created_at) values (?, ?, ?, ?) returning id;',
                    (sext.str_to_date(date_prop[0].text)[0], date_prop[0].text, date_accuracy, datetime.datetime.now()))
                birthdate_id = cur.fetchone()[0]
                cur.execute('Update "agents" set birthdate_id = ? where id = ?;', (birthdate_id, agent_id))
            if date_type == 'deathdate':
                cur.execute('Insert into "dates" (date, date_string, accuracy, created_at) values (?, ?, ?, ?) returning id;',
                    (sext.str_to_date(date_prop[0].text)[0], date_prop[0].text, date_accuracy, datetime.datetime.now()))
                deathdate_id = cur.fetchone()[0]
                cur.execute('Update "agents" set deathdate_id = ? where id = ?;', (deathdate_id, agent_id))

        # write geography
        for geoname_prop in resource.iter('geoname-prop'):
            geoname_type = sext.fix_type_name(geoname_prop.get('name'))
            if geoname_type == 'birthplace':
                gnid = geoname_prop[0].text
                add_place(cur, sext.resolve_geonames(gnid, 'salsah-export'), agent_id, 'agents', 'birthplace_id')
            if geoname_type == 'deathplace':
                gnid = geoname_prop[0].text
                add_place(cur, sext.resolve_geonames(gnid, 'salsah-export'), agent_id, 'agents', 'deathplace_id')

def populate_comment(cur, comments, table, entity, id):
    for comment in comments:
        cur.execute('Insert into "comments" (comment) values (?) returning id;', tuple([comment]))
        comment_id = cur.fetchone()[0]
        cur.execute('Insert into "'+table+'" ('+entity+'_id, comment_id) values (?, ?);', (id, comment_id))

def populate_album(cur):
    print('Populating Albums')

    parser = et.XMLParser(remove_blank_text=True)
    salsah_export = et.parse('salsah-export/Album/sgv.xml', parser)
    root = salsah_export.getroot();
    
    for resource in root.iter('resource'):
        sql = '''Insert into "albums" (salsah_id, label, title, signature, description) 
                    values (?, ?, ?, ?, ?) returning id;'''
        values = [
            resource.get('id'), 
            resource.get('label'),
            "", "", ""
        ]
        comment = []
        indexing = []

        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))

            if property_type == 'title':
                values[2] = sext.rmnl(text_prop[0].text)
            if property_type == 'signature':
                values[3] = sext.rmnl(text_prop[0].text)
            if property_type == 'description_rt':
                values[4] = sext.rmnl(text_prop[0].text)
            if property_type == 'salsah_comment':
                for c in text_prop.iter('text'):
                    comment.append(c.text)
            if property_type == 'indexing':
                for i in text_prop.iter('text'):
                    indexing.append(i.text)

        cur.execute(sql, tuple(values))
        album_id = cur.fetchone()[0]

        populate_comment(cur, comment, 'album_comment', 'album', album_id)

        for date_prop in resource.iter('date-prop'):
            for date in date_prop:
                date_accuracy = get_date_accuracy(date_prop[0].text)
                cur.execute('Insert into "dates" (date, date_string, accuracy, created_at) values (?, ?, ?, ?) returning id;',
                    (sext.str_to_date(date.text)[0], date.text, date_accuracy, datetime.datetime.now()))
                date_id = cur.fetchone()[0]
                cur.execute('Update "albums" set date_id = ? where id = ?;', (album_id, date_id))

        for resptr_prop in resource.iter('resptr-prop'):
            property_type = sext.fix_type_name(resptr_prop.get('name'))
            if property_type == 'people':
                for agent in resptr_prop:
                    add_agent(cur, agent.text, album_id, 'albums')
            
            if property_type == 'in_collection':
                add_to_collection(cur, 'album_collection', 'album', resptr_prop[0].text, album_id)

        # write list props
        for list_prop in resource.iter('list-prop'):
            list_type = sext.fix_type_name(list_prop.get('name'))
            
            list_value = list_props.get(list_prop[0].text)
            list_comment = list_prop[0].get('comment')

            if list_comment:
                prop_comment = list_comment
            else:
                prop_comment = ''

            if list_value:
                prop_value = list_value
            else:
                prop_value = list_prop[0].text
            
            if list_type == 'objecttype':
                if resptr_prop[0].text != '0':
                    object_type_id = add_property(cur, 'object_types', prop_value, prop_comment)
                    cur.execute('Update "albums" set object_type_id = ? where id = ?;', (object_type_id, album_id))

def populate_collections(cur):
    print('Populating Collections')

    parser = et.XMLParser(remove_blank_text=True)
    salsah_export = et.parse('salsah-export/Collection/sgv.xml', parser)
    root = salsah_export.getroot();
    
    for resource in root.iter('resource'):
        sql = '''Insert into "collections"
                    (salsah_id, label, signature, description, default_image, embedded_video, origin, created_at) 
                    values (?, ?, ?, ?, ?, ?, ?, ?)
                    returning id;'''
        values = [
            resource.get('id'), 
            resource.get('label'),
            "", "", "", "", "salsah",
            datetime.datetime.now()
        ]
        comment = []
        indexing = []

        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))

            if property_type == 'signature':
                values[2] = sext.rmnl(text_prop[0].text)
            if property_type == 'description_rt':
                values[3] = sext.rmnl(text_prop[0].text)
            if property_type == 'salsah_comment':
                for c in text_prop.iter('text'):
                    comment.append(c.text)
            if property_type == 'default_image':
                values[4] = text_prop[0].text
            if property_type == 'embedded_video':
                values[5] = text_prop[0].text
            if property_type == 'indexing':
                for i in text_prop.iter('text'):
                    indexing.append(i.text)
            if property_type == 'indexing_rt':
                for i in text_prop.iter('text'):
                    indexing.append(i.text)

        cur.execute(sql, tuple(values))
        collection_id = cur.fetchone()[0]

        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))

            if property_type == 'title':
                for t in text_prop.iter('text'):
                    cur.execute('Insert into "alt_labels" (label, created_at) values (?, ?) returning id;', tuple([t.text, datetime.datetime.now()]))
                    alt_label_id = cur.fetchone()[0]
                    cur.execute('Insert into "alt_label_collection" (alt_label_id, collection_id) values (?, ?);', tuple([alt_label_id, collection_id]))
            
            if property_type == 'literature':
                for literature in text_prop.iter('text'):
                    cur.execute('Insert into "literatures" (label, created_at) values (?, ?) returning id;', tuple([literature.text, datetime.datetime.now()]))
                    literature_id = cur.fetchone()[0]
                    cur.execute('Insert into "collection_literature" (collection_id, literature_id) values (?, ?);', tuple([collection_id, literature_id]))

        populate_comment(cur, comment, 'collection_comment', 'collection', collection_id)

        for date_prop in resource.iter('date-prop'):
            for date in date_prop:
                date_accuracy = get_date_accuracy(date_prop[0].text)
                cur.execute('Insert into "dates" (date, date_string, accuracy, created_at) values (?, ?, ?, ?) returning id;',
                    (sext.str_to_date(date.text)[0], date.text, date_accuracy, datetime.datetime.now()))
                date_id = cur.fetchone()[0]
                cur.execute('Update "collections" set date_id = ? where id = ?;', (collection_id, date_id))

        for resptr_prop in resource.iter('resptr-prop'):
            property_type = sext.fix_type_name(resptr_prop.get('name'))
            if property_type == 'people':
                for person in resptr_prop:
                    add_agent(cur, person.text, collection_id, 'collections')

def populate_from_archive(cur, conn, project):
    print('Populating '+project)

    # setup script
    project_dir = 'salsah-export/Images/'+project
    
    xml_path = '/'.join((project_dir, 'sgv.xml'))

    # read and parse exported salsah xml 
    parser = et.XMLParser(remove_blank_text=True)
    salsah_export = et.parse(xml_path, parser)
    root = salsah_export.getroot();

    '''
        Commence population
    '''
    # iterate over all <resource>
    total_res = sum(1 for _ in root.iter('resource'))
    current_res = 0
    print('Found '+str(total_res)+' ressources in '+project)

    sql = 'Insert into "images" (salsah_id, title, origin, signature, oldnr, base_path, created_at) values (?, ?, ?, ?, ?, ?, ?);'
    batch_values = []

    print('Adding them')
    for resource in root.iter('resource'):
        values = [resource.get('id'), "", "salsah", "", "", project, datetime.datetime.now()]

        # write simple text props
        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))

            if property_type == 'title':
                values[1] = sext.rmnl(text_prop[0].text)
            if property_type == 'signature':
                values[3] = sext.rmnl(text_prop[0].text)
            if property_type == 'oldnr':
                values[4] = sext.rmnl(text_prop[0].text)

            # write keywords (subject)
            '''if property_type == 'subject':
                for keyword in text_prop.iter('text'):
                    keywords = keyword.text.replace('[', '').replace(']', '')
                    for k in keywords.split(','):
                        cur.execute('Select id from "keywords" where label = ?;', (k.strip(),))
                        fetch = cur.fetchone()

                        if fetch:
                            cur.execute('Insert into "image_keyword" (image_id, keyword_id) values (?, ?);', (image_id, fetch[0]))'''

        batch_values.append(tuple(values))

    cur.executemany(sql, batch_values)

    for resource in root.iter('resource'):
        current_res += 1

        if not current_res % 100:
            print(str(current_res)+'/'+str(total_res)+' resources for '+project+' populated')
        
        cur.execute('Select id from "images" where salsah_id = '+resource.get('id')+';')
        image_id = cur.fetchone()[0]

        comment = []

        # convert subject to keyword
        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))

            if property_type == 'subject':
                for keyword in text_prop.iter('text'):
                    keywords = keyword.text.replace('[', '').replace(']', '')
                    for k in keywords.split(','):
                        cur.execute('Select id from "keywords" where label = ?;', (k.strip(),))
                        fetch = cur.fetchone()

                        if fetch:
                            cur.execute('Insert into "image_keyword" (image_id, keyword_id) values (?, ?);', (image_id, fetch[0]))
            if property_type == 'salsah_comment':
                for c in text_prop.iter('text'):
                    comment.append(c.text)

        # write comment
        populate_comment(cur, comment, 'comment_image', 'image', image_id)

        # write dates
        for date_prop in resource.iter('date-prop'):
            dates = sext.str_to_date(date_prop[0].text)
            date = dates[0]
            if len(dates) > 1:
                end_date = dates[1]
            else:
                end_date = ''

            date_accuracy = get_date_accuracy(date_prop[0].text)
            cur.execute('Insert into "dates" (date, end_date, date_string, accuracy, created_at) values (?, ?, ?, ?, ?) returning id;',
                    (date, end_date, date_prop[0].text, date_accuracy, datetime.datetime.now()))
            date_id = cur.fetchone()[0]
            cur.execute('Update "images" set date_id = ? where id = ?;', (date_id, image_id))
        
        # write resptr props
        for resptr_prop in resource.iter('resptr-prop'):
            property_type = sext.fix_type_name(resptr_prop.get('name'))

            if property_type == 'tag':
                for resptr in resptr_prop.iter('resptr'):
                    if resptr.text != '0':
                        resptr_id = resptr.text
                        cur.execute('Select id from "keywords" where salsah_id = ?;', (resptr_id,))
                        fetch = cur.fetchone()

                        if fetch:
                            cur.execute('Insert into "image_keyword" (image_id, keyword_id) values (?, ?);', (image_id, fetch[0]))
            
            # write people
            # TODO: differenciate people, institution and families
            if property_type in ['person', 'content']:
                for resptr in resptr_prop.iter('resptr'):
                    add_agent(cur, resptr.text, image_id, property_type)
            
            if property_type == 'copyright':
                agent_id = add_agent(cur, resptr.text, image_id, 'copyright')
                cur.execute('Update "images" set copyright_id = ? where id = ?;', (image_id, agent_id))
            
            if property_type == 'in_collection':
                add_to_collection(cur, 'collection_image', 'image', resptr_prop[0].text, image_id)

        # write list props
        for list_prop in resource.iter('list-prop'):
            list_type = sext.fix_type_name(list_prop.get('name'))
            
            list_value = list_props.get(list_prop[0].text)
            list_comment = list_prop[0].get('comment')

            if list_comment:
                prop_comment = list_comment
            else:
                prop_comment = ''

            if list_value:
                prop_value = list_value
            else:
                prop_value = list_prop[0].text

            if list_type == 'model':
                if resptr_prop[0].text != '0':
                    model_type_id = add_property(cur, 'model_types', prop_value, prop_comment)
                    cur.execute('Update "images" set model_type_id = ? where id = ?;', (model_type_id, image_id))
            
            if list_type == 'format':
                if resptr_prop[0].text != '0':
                    format_id = add_property(cur, 'formats', prop_value, prop_comment)
                    cur.execute('Update "images" set format_id = ? where id = ?;', (format_id, image_id))
            
            if list_type == 'objecttype':
                if resptr_prop[0].text != '0':
                    object_type_id = add_property(cur, 'object_types', prop_value, prop_comment)
                    cur.execute('Update "images" set object_type_id = ? where id = ?;', (object_type_id, image_id))
        
        # write geography
        for geoname_prop in resource.iter('geoname-prop'):
            gnid = geoname_prop[0].text
            add_place(cur, sext.resolve_geonames(gnid, 'salsah-export'), image_id, 'images', 'place_id')

    print('Adding image references')
    current_res = 0
    reference_values = []
    for resource in root.iter('resource'):
        current_res += 1

        if not current_res % 100:
            print('Added references to '+str(current_res)+'/'+str(total_res)+' resources for '+project)
        
        ref_img_ids = resource.xpath("resptr-prop[@name=':hasRef_img']/resptr/text()")
        verso_ids = resource.xpath("resptr-prop[@name='hasverso']/resptr/text()")

        if len(ref_img_ids) or len(verso_ids):
            cur.execute('Select id from "images" where salsah_id = '+resource.get('id')+';')
            image_a_id = cur.fetchone()[0]

        if len(ref_img_ids):
            for ref_img_id in ref_img_ids:
                cur.execute('Select id from "images" where salsah_id = '+ref_img_id+';')
                image_b = cur.fetchone()
                if image_b:
                    reference_values.append((1, image_a_id, image_b[0]))
            
        if len(verso_ids):
            cur.execute('Select id from "images" where salsah_id = '+verso_ids[0]+';')
            verso = cur.fetchone()
            if verso:
                reference_values.append((2, image_a_id, verso[0]))

    cur.executemany('Insert into "image_image" (type, image_a_id, image_b_id) values (?, ?, ?);', reference_values)

    print('Populated all '+str(current_res)+' resources of '+project)

def add_property(cur, col, label, comment):
    cur.execute('Select id from "'+col+'" where label = ?;', (label,))
    fetch = cur.fetchone()

    if not fetch:
        cur.execute('Insert into "'+col+'" (label, comment) values (?, ?) returning id;', (label, comment))
        id = cur.fetchone()[0]
    else:
        id = fetch[0]
    
    return id

def add_keyword(cur, keyword, image_id):
    keyword_id = add_property(cur, 'keywords', keyword, '')

    cur.execute('Insert into "image_keyword" (image_id, keyword_id) values (?, ?);', (image_id, keyword_id))

def add_agent(cur, salsah_agent_id, id, property_type):
    if salsah_agent_id != 0:
        cur.execute('Select id from "agents" where salsah_id = ?;', (salsah_agent_id,))
        fetch = cur.fetchone()

        if fetch:
            agent_id = fetch[0]
        else:
            agent = et.Element('person')
            agent.text = salsah_agent_id
            resource = sext.resolve_prop(agent, 'salsah-export')

            sql = '''Insert into "agents" (salsah_id, name) 
                    values (?, ?) returning id;'''
            values = [
                salsah_agent_id, 
                resource['resource_info']['firstproperty']
            ]

            cur.execute(sql, tuple(values))
            agent_id = cur.fetchone()[0]

        if property_type == 'person':
            cur.execute('Insert into "agent_image" (agent_id, image_id) values (?, ?);', (agent_id, id))

        if property_type == 'content':
            cur.execute('Insert into "agent_image" (agent_id, image_id) values (?, ?);', (agent_id, id))

        if property_type == 'albums':
            cur.execute('Insert into "agent_album" (agent_id, album_id) values (?, ?);', (agent_id, id))

        if property_type == 'collections':
            cur.execute('Insert into "agent_collection" (agent_id, collection_id) values (?, ?);', (agent_id, id))
        
        return agent_id
    
    return -1

def add_place(cur, geonames_data, id, entity, col):
    geonames_id = geonames_data.findtext('geonameId', default = '')
    cur.execute('Select id from "places" where geonames_id = ?;', (geonames_id,))
    fetch = cur.fetchone()

    if not fetch:
        label = geonames_data.findtext('name', default = '')
        geonames_uri = 'http://sws.geonames.org/'+geonames_id

        geonames_lat = geonames_data.findtext('lat', default = 0)
        geonames_lng = geonames_data.findtext('lng', default = 0)
        
        geonames_code = geonames_data.findtext('fcode', default = '')
        geonames_code_name = geonames_data.findtext('fcodeName', default = '')
        geonames_division_level = '1'
        wiki_uri = ''
        geometry = ''

        
        for alternatename in geonames_data.iterfind('alternateName'):
            if alternatename.get('lang') == 'link' and 'en.wikipedia.org' in alternatename.text:
                wiki_uri = alternatename.text
            if alternatename.get('isPreferredName') and alternatename.get('lang') == 'de':
                label = alternatename.text

        if geonames_data.findtext('adminCode2', default = 0):
            geonames_division_level = '2'
        if geonames_data.findtext('adminCode3', default = 0):
            geonames_division_level = '3'
        if geonames_data.findtext('adminCode4', default = 0):
            geonames_division_level = '4'
        if geonames_data.findtext('adminCode5', default = 0):
            geonames_division_level = '5'

        '''
        if geonames_division_level == '1':
            canton = label.replace('Kanton ', '')
            for feature in cantons['features']:
                if feature['properties']['NAME'] == canton:
                    geometry = json.dumps(feature['geometry'])
        '''

        if geonames_division_level == '2':
            for feature in districts['features']:
                if str(feature['properties']['BEZIRKSNUM']) == str(geonames_data.findtext('adminCode2')):
                    geometry = json.dumps(feature['geometry'])

        if geonames_division_level == '3':
            for feature in territories['features']:
                if str(feature['properties']['BFS_NUMMER']) == str(geonames_data.findtext('adminCode3')):
                    geometry = json.dumps(feature['geometry'])

        cur.execute(
            'Insert into "places" (label, geonames_id, geonames_uri, geonames_code, geonames_code_name, geonames_division_level, wiki_uri, geometry, latitude, longitude, origin) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "salsah") returning id;',
            (label, geonames_id, geonames_uri, geonames_code, geonames_code_name, geonames_division_level, wiki_uri, geometry, geonames_lat, geonames_lng)
        )
        place_id = cur.fetchone()[0]
    else:
        place_id = fetch[0]

    cur.execute('Update "'+entity+'" set '+col+' = ? where id = ?;', (place_id, id))

def add_to_collection(cur, table, entity, collection_salsah_id, entity_id):
    if collection_salsah_id != 0:
        cur.execute('Select id from "collections" where salsah_id = ?;', (collection_salsah_id,))
        fetch = cur.fetchone()

        if fetch:
            collection_id = fetch[0]
            cur.execute(
                'Insert into "'+table+'" ('+entity+'_id, collection_id) values (?, ?);',
                (entity_id, collection_id)
            )

def get_date_accuracy(date_string):
    accuracy = 0
    count = date_string.count('-')

    if date_string.count(':') == 4:
        count = int(count / 2)

    if count == 0:
        accuracy = 3
    if count == 1:
        accuracy = 2
    if count == 2:
        accuracy = 1

    return accuracy

if __name__ == '__main__':
    main()
