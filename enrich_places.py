'''
2021 Participatory Image Archives
Contact: Adrian Demleitner, adrian.demleitner@unibas.ch

https://github.com/Participatory-Image-Archives/salsah-export
'''

import os, json, sqlite3, requests, time
from lxml import etree as et

with open('swiss_boundaries/swissBOUNDARIES3D_1_3_TLM_BEZIRKSGEBIET.geojson') as f:
    districts = json.load(f)
with open('swiss_boundaries/swissBOUNDARIES3D_1_3_TLM_HOHEITSGEBIET.geojson') as f:
    territories = json.load(f)

def main():
    # db connection
    conn = sqlite3.connect("/Users/adriandemleitner/Repositories/pia-database/database/pia.sqlite")
    cur = conn.cursor()

    geonames_dir = 'salsah-export/geonames'
    if not os.path.exists(geonames_dir):
        os.makedirs(geonames_dir)
    parser = et.XMLParser(remove_blank_text=True)

    cur.execute("Select * from places where geonames_id IS NOT NULL")

    places = cur.fetchall()

    for place in places:
        print('Enriching', place[4], 'with Geonames ID', place[9])

        geonames_id = place[9]
        geonames_file = '/'.join((geonames_dir, (str(geonames_id)+'.xml')))
        geonames_data = None

        if not os.path.isfile(geonames_file):
            response = requests.get('http://api.geonames.org/get?geonameId={}&username=thgie'.format(geonames_id))

            with open(geonames_file, 'wb') as file:
                file.write(response.content)

        geonames_data = et.parse(geonames_file, parser)

        if geonames_data.getroot()[0].get('message'):
            print(geonames_data.getroot()[0].get('message'))
            os.remove(geonames_file)
            break

        enrich_place(cur, geonames_data.getroot())

        time.sleep(0.025)
    
    conn.commit()
    cur.close()

def enrich_place(cur, geonames_data):
    geonames_id = geonames_data.findtext('geonameId', default = '')
    cur.execute('Select id from "places" where geonames_id = ?;', (geonames_id,))
    fetch = cur.fetchone()

    if fetch:
        id = fetch[0]
        
        label = geonames_data.findtext('name', default = '')
        geonames_uri = 'http://sws.geonames.org/'+geonames_id

        geonames_altitude = geonames_data.findtext('elevation', default = 0)

        geonames_lat = geonames_data.findtext('lat', default = 0)
        geonames_lng = geonames_data.findtext('lng', default = 0)
        
        geonames_code = geonames_data.findtext('fcode', default = '')
        geonames_code_name = geonames_data.findtext('fcodeName', default = '')
        geonames_division_level = '1'

        geometry = ''
        wiki_uri = ''

        
        for alternatename in geonames_data.iterfind('alternateName'):
            if alternatename.get('lang') == 'link' and 'en.wikipedia.org' in alternatename.text:
                wiki_uri = alternatename.text
            if alternatename.get('isPreferredName') and alternatename.get('lang') == 'de':
                label = alternatename.text

        if geonames_data.findtext('adminCode2', default = 0):
            geonames_division_level = '2'
        if geonames_data.findtext('adminCode3', default = 0):
            geonames_division_level = '3'
        if geonames_data.findtext('adminCode4', default = 0):
            geonames_division_level = '4'
        if geonames_data.findtext('adminCode5', default = 0):
            geonames_division_level = '5'

        '''
        if geonames_division_level == '1':
            canton = label.replace('Kanton ', '')
            for feature in cantons['features']:
                if feature['properties']['NAME'] == canton:
                    geometry = json.dumps(feature['geometry'])
        '''

        if geonames_division_level == '2':
            for feature in districts['features']:
                if str(feature['properties']['BEZIRKSNUM']) == str(geonames_data.findtext('adminCode2')):
                    geometry = json.dumps(feature['geometry'])

        if geonames_division_level == '3':
            for feature in territories['features']:
                if str(feature['properties']['BFS_NUMMER']) == str(geonames_data.findtext('adminCode3')):
                    geometry = json.dumps(feature['geometry'])

        cur.execute(
            'Update "places" set label = ?, geonames_code = ?, geonames_code_name = ?, geonames_division_level = ?, geometry = ?, latitude = ?, longitude = ?, altitude = ?, wiki_uri = ? where id = ?;',
            (label, geonames_code, geonames_code_name, geonames_division_level, geometry, geonames_lat, geonames_lng, geonames_altitude, wiki_uri, id)
        )

if __name__ == '__main__':
    main()
