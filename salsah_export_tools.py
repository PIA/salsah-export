import os, json, urllib.request, requests
from datetime import datetime
from lxml import etree as et


# resolve property, either via webcall or saved json file
def resolve_prop(resptr, project_dir):
    properties_dir =    '/'.join((project_dir, 'properties'))
    if not os.path.exists(properties_dir):
        os.makedirs(properties_dir)
    
    resptr_id = resptr.text
    property_file = '/'.join((properties_dir, (resptr_id+'.json')))
    property_data = None

    # reduce json grabbing by writing results to reusable files
    if not os.path.isfile(property_file):

        with urllib.request.urlopen('https://www.salsah.org/api/resources/{}?reqtype=info'.format(resptr_id)) as url:

            property_data = json.loads(url.read().decode())

            with open(property_file, 'w') as json_file:
                json.dump(property_data, json_file)
    else:
        with open(property_file) as f:
            property_data = json.load(f)

    return property_data

def resolve_geonames(id, project_dir):

    geonames_salsah_dir = 'salsah-export/geonames_salsah'
    if not os.path.exists(geonames_salsah_dir):
        os.makedirs(geonames_salsah_dir)
    
    geonames_salsah_file = '/'.join((geonames_salsah_dir, (id+'.json')))
    geonames_salsah_data = None

    # reduce json grabbing by writing results to reusable files
    if not os.path.isfile(geonames_salsah_file):

        with urllib.request.urlopen('https://www.salsah.org/api/geonames/{}?reqtype=info'.format(id)) as url:

            geonames_salsah_data = json.loads(url.read().decode())

            with open(geonames_salsah_file, 'w') as json_file:
                json.dump(geonames_salsah_data, json_file)
    else:
        with open(geonames_salsah_file) as f:
            geonames_salsah_data = json.load(f)

    geonames_salsah = geonames_salsah_data['nodelist'].pop()
    
    geonames_id = geonames_salsah['name'].split(':')[1]

    geonames_dir = 'salsah-export/geonames'
    if not os.path.exists(geonames_dir):
        os.makedirs(geonames_dir)
    
    geonames_file = '/'.join((geonames_dir, (str(geonames_id)+'.xml')))
    geonames_data = None

    # reduce json grabbing by writing results to reusable files
    if not os.path.isfile(geonames_file):
        response = requests.get('http://api.geonames.org/get?geonameId={}&username=thgie'.format(geonames_id))
        with open(geonames_file, 'wb') as file:
            file.write(response.content)

    parser = et.XMLParser(remove_blank_text=True)
    geonames_data = et.parse(geonames_file, parser)

    if geonames_data.findtext('name', default = 'None') == 'None':
        print(geonames_id)

    return geonames_data.getroot()

# property type name needs some fixing due to different styles
def fix_type_name(name):
    # :hasPerson -> person, hasVerso -> verso
    # cutoff of :has/has to make xml human-readable
    type_cutoff = 4
    if name[0] != ':':
        type_cutoff = 3
    # is salsah comment
    elif 'salsah:comment' in name:
        type_cutoff = 1
    
    return name[type_cutoff:].lower().replace(':', '_')

# find all occurences of a specific node
# via https://hackersandslackers.com/extract-data-from-complex-json-python/
def json_extract(obj, key):
    """Recursively fetch values from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                if k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    values = extract(obj, arr, key)
    return values

def rmnl(text):
    return text.replace('\n', ' ').replace('\r', '')

def str_to_date(date_str):
    date_arr = []

    date_str = date_str.replace('GREGORIAN:', '')
    date_str = date_str.replace('CE:', '')

    for date in date_str.split(':'):
        if(clean_date(date)):
            date_arr.append(clean_date(date))
    
    return date_arr

def clean_date(date_str):
    try:
        date = datetime.strptime(date_str, '%Y-%m-%d').date()
    except ValueError:
        date = False

    if not date:
        try:
            date = datetime.strptime(date_str, '%Y-%m').date()
        except ValueError:
            date = False

    if not date:
        try:
            date = datetime.strptime(date_str, '%Y').date()
        except ValueError:
            date = False

    return date