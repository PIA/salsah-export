# salsah-export
Export scripts in Python for extracting metadata and data from [Salsah](https://salsah.org), a virtual research environment developed by the Data & Service Center for the Humanities (DaSCH).

This GitHub repository was created in the context of the _Participatory Knowledge Practices in Analogue and Digital Image Archives_ (PIA) project and specifically to extract content from three collections of the Swiss Society for Folklore Studies/Schweizerische Gesellschaft für Volkskunde (SSFS/SGV).

## TOC
- [Cloning and installation](#cloning-and-installation)
- [Usage](#usage)
    - [Export metadata from Salsah](#export-metadata-from-salsah)
    - [Resolve Salsah metadata](#resolve-salsah-metadata)
    - [Populate PIA database](#populate-pia-database)
- [Background](#background)

## Cloning and installation

Download ZIP or clone the repository

```bash
git clone Participatory-Image-Archives/salsah-export
```

To install the requirements:

```bash
pip3 install -r requirements.txt
```

## Usage
### Export metadata from Salsah
This step uses the `salsah2xml.py` script to export the data from the salsah database into a XML file. The script was originally provided by Lukas Rosenthaler of the Digital Humanities Lab, Basel.

#### Command

```bash
python3 salsah2xml.py --project {shortname} --shortcode {shortcode} --start {n} --nrows {n} --download --write-metadata --filter {shortname}:{resourcetype}={id} --request-image-type {jpg|jp2|png|tif} --restype {shortname}:{resourcetype} --batch-size {n} {USER} {PASSWORD} https://www.salsah.org
```

#### Main arguments

- `project`: shortname or ID of the project (sgv)
- `shortcode`: Knora shortcode (_4102_ for the SGV)
- `--start`: start at record with given number
- `--nrows`: amount of records
- `--download`: records are being downloaded
- `--write-metadata`: associated metadata are being written in an XML file
- `--filter`: supposedly a filter by resource type according to one of the Salsah ontologies
- `--request-image-type`: request a specified image format (JPEG, JPEG2000*, PNG or TIFF)
- `--restype`: download a specific resource type
- `--batch-size`: batch size to split download into (1000 by default)

**NB: If JPEG2000 are requested from Salsah, only regular JPEG will be delivered.** 

The different resource types within the SGV ontology are: 
- sgv:album
- sgv:collection
- sgv:image
- sgv:film
- sgv:sequence
- sgv:subject
- sgv:tonbildschau

The filter argument for collections can be used as follows: `sgv:in_collection={ID}`

Collection | Short Name | ID
--- | --- | ---
Atlas der Schweizerischen Volkskunde (ASV) | SGV_05 | 344301
Familie Kreis | SGV_10 | 344305
Ernst Brunner | SGV_12 | 421714

The collections' ID can be found at the following URL on the Salsah API: https://data.dasch.swiss/api/resources?restype_id=67

#### Outputs
Below is a command example for downloading five images (JP2) and their associated metadata from the SGV_10 Familie Kreis collection:

```bash
python3 salsah2xml.py --project sgv --shortcode 4102 --start 3 --nrows 5 --download --write-metadata --filter sgv:in_collection=344305 --request-image-type jp2 --restype sgv:image {USER} {PASSWORD} https://www.salsah.org
```

After running the above command, the recors and their associated metadata will be downloaded in a `sgv.dir` folder (by default) structured in the following manner:
- assets - TBD
- images
- sgv.json (SGV Ontology)
- sgv.xml (metadata of the downloaded records)
<img width="721" alt="Screenshot 2021-04-28 at 12 08 41" src="https://user-images.githubusercontent.com/17925031/116386870-7fb33880-a81a-11eb-9cf1-b041daaf789b.png">

To get other metadata objects you can use calls like the following, in the case of Persons.

```bash
python3 salsah2xml.py --project sgv --shortcode 4102 --start 0 --nrows 2519 --write-metadata --batch-size 500 --restype sgv:persons {USER} {PASSWORD} https://www.salsah.org
```

### Resolve Salsah metadata
This step works with `resolve_properties.py` to resolve the missing properties in the XML file and adds the capability to convert the resolved XML to a csv file. This script needs the exported metadata descriped in [Export metadata from Salsah](#export-metadata-from-salsah). See [Unresolved Salsah metadata](#unresolved-salsah-metadata) for more information.

#### Command
```bash
python3 resolve_properties.py --project sgv --csv True
```

#### Arguments
- `--project`: Reads the folder structure created by `salsah2xml.py`
- `--csv True`: Write the resolved and cleaned XML to a CSV file

The script generates a second XML file with the properties resolved by adding the following structure along the unresolved property id.

#### Outputs
Except for the `svg:Image` resource type where only PIA-related collections were extracted (SGV_10 and SGV_12), all of the SGV metadata have been exported using the above scripts. The available XML and CSV files can be found in the appropriate folders of the PIA Data Model GitHub repository: https://github.com/Participatory-Image-Archives/pia-data-model/tree/main/salsah-export

It this directory can also be found the `Geonames` and `properties` in JSON as well as a CSV which comprises all ASV places (`asv_places.csv` - SGV_05) and the current SGV ontology (`sgv.json`).

### Populate PIA database
The purpose of `populate_database.py` is to populate a PostgeSQL database with the extracted metadata from Salsah. This script needs the exported metadata descriped in [Export metadata from Salsah](#export-metadata-from-salsah).

#### Setup database schema
Here are the main steps: 

1. Create a PostgreSQL database
2. Import the [PIA database model](https://github.com/Participatory-Image-Archives/pia-data-model/blob/main/dbml%20model/pia_datamodel.sql) (SQL file)
3. Download the [Salsah Export](https://github.com/Participatory-Image-Archives/pia-data-model/tree/main/salsah-export) and put the unzipped folder in the root directory
4. Modify [line 30](https://github.com/Participatory-Image-Archives/salsah-export/blob/6a086bcdc4d9fa84d2aaaa6ca52c43184ea803dc/populate_database.py#L30) according to the created database and its credentials

After this you are able to run the population script.

#### Command
```bash
python3 populate_database.py
```

## Background
### Unresolved Salsah metadata
In the provided XML file, some properties are unresolved. For example

```xml
<resptr-prop name=":hasContent">
    <resptr>375013</resptr>
    <resptr>492859</resptr>
</resptr-prop>
```

The salsah.org API provides a JSON object for each property with their respective metadata via

`https://www.salsah.org/api/resources/{ID}?reqtype=info` 

or respectively for [GeoNames](https://www.geonames.org/) IDs:

`https://www.salsah.org/api/geonames/{ID}?reqtype=info`

See [Salsah endpoints](#salsah-endpoints) for more information.

Of interest are especially the value field `firstproperty` as well the user-given label `restype_description`. After successfully grabbing the metadata XML file, a second script will resolve the properties. Call the script on the same level as `salsah2xml.py`

The script also parses and resolves the other property types:

- `text-prop`
- `list-prop`

### Salsah endpoints
As background, here are the different endpoints of Salsah.

Type | Endpoint
--- | ---
Salsah UI | `https://data.dasch.swiss/resources/{ID}`
Salsah API (JSON) | `https://data.dasch.swiss/api/resources/{ID}`
Salsah API (HTML Preview) | `https://data.dasch.swiss/api/properties.html/{ID}`
Salsah Image API | `https://data.dasch.swiss/core/sendlocdata.php?qtype=full&reduce=5&res={ID}`

It is to be noted that what is on https://www.salsah.org is equivalent (and will be later resolved) to what is on https://data.dasch.swiss. 
