'''
2021 Participatory Image Archives
Contact: Adrian Demleitner, adrian.demleitner@unibas.ch

https://github.com/Participatory-Image-Archives/salsah-export
'''

import argparse, requests
import csv, json, urllib.request

import os
from lxml import etree as et

import salsah_export_tools as sext

def main():
    # setup script
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-P', '--project', help='shortname or ID of project')
    arg_parser.add_argument('-csv', '--csv', help='write resolved and cleaned XML to a CSV file')
    arg_parser.set_defaults(csv=False)
    args = arg_parser.parse_args()

    project = args.project
    project_dir = project+'.dir'
    project_xml = project+'.xml'
    project_json = project+'.json'
    project_csv = project+'.csv'
    
    xml_path =          '/'.join((project_dir, project_xml))
    xml_resolved_path = '/'.join((project_dir, project+'_resolved.xml'))
    json_path =         '/'.join((project_dir, project_json))
    csv_path =          '/'.join((project_dir, project_csv))
    properties_dir =    '/'.join((project_dir, 'properties'))
    geonames_dir =      '/'.join((project_dir, 'geonames_salsah'))

    if not os.path.exists(properties_dir):
        os.makedirs(properties_dir)

    if not os.path.exists(geonames_dir):
        os.makedirs(geonames_dir)

    # read and parse exported salsah xml 
    parser = et.XMLParser(remove_blank_text=True)
    salsah_export = et.parse(xml_path, parser)
    root = salsah_export.getroot();

    # read project ontology and parse lists for easier access
    with open(project_json) as f:
        ontology = json.load(f)

    lists = sext.json_extract(ontology, 'nodes')
    list_props = {}

    for l in lists:
        for p in l:
            list_props[p['name']] = p['labels']['de']
    
    # iterate over all <resource>
    for resource in root.iter('resource'):

        '''
        resolve <resptr-prop>
        '''
        # iterate over all <resptr-prop>
        for resptr_prop in resource.iter('resptr-prop'):
        # iterate over all <resptr>
        # resolve property via https://www.salsah.org/api/resources/{resptr_id}?reqtype=info

            property_type = sext.fix_type_name(resptr_prop.get('name'))
            resolved_prop = et.SubElement(resource, property_type)

            if len(resptr_prop.getchildren()) > 1:
                for idx, resptr in enumerate(resptr_prop.iter('resptr')):
                    if resptr.text != '0':
                        p_data = sext.resolve_prop(resptr, project_dir)
                        prop = et.SubElement(resolved_prop, 'property')
                        prop.set('index_'+str(idx), p_data['resource_info']['firstproperty'])
            else:
                if resptr_prop[0].text != '0':
                    p_data = sext.resolve_prop(resptr_prop[0], project_dir)
                    resolved_prop.text = p_data['resource_info']['firstproperty']

            resptr_prop.getparent().remove(resptr_prop)


        '''
        resolve <text-prop>
        '''
        # iterate over all <text-prop>
        # replace element with simpler version
        for text_prop in resource.iter('text-prop'):
            property_type = sext.fix_type_name(text_prop.get('name'))
            prop = et.SubElement(resource, property_type)

            if len(text_prop.getchildren()) > 1:
                for idx, comment in enumerate(text_prop.iter('text')):
                    comment_prop = et.SubElement(prop, 'property')
                    comment_prop.set('index_'+str(idx), comment.text.replace('\n', ' ').replace('\r', ''))
            else:
                prop.text = text_prop[0].text.replace('\n', ' ').replace('\r', '')
            
            text_prop.getparent().remove(text_prop)

        '''
        resolve <list-prop>
        '''
        # iterate over all <list-prop>
        for list_prop in resource.iter('list-prop'):
            list_type = sext.fix_type_name(list_prop.get('name'))
            prop = et.SubElement(resource, list_type)

            list_value = list_props.get(list_prop[0].text)
            list_comment= list_prop[0].get('comment')

            if list_value:
                prop.text = list_value
            else:
                prop.text = list_prop[0].text
                prop.set('unresolved', 'True')

            if list_comment:
                prop.set('comment', list_comment)

            list_prop.getparent().remove(list_prop)
        
        '''
        resolve <date-prop>
        '''
        # iterate over all <date-prop>
        for date_prop in resource.iter('date-prop'):

            # cleanup
            date_val = date_prop[0].text.replace('GREGORIAN:', '').replace('CE:', '')

            # either write date directly or daterange if present
            if date_val.count(':') == 1:
                date_range = et.SubElement(resource, 'daterange')
                date_range_start = et.SubElement(date_range, 'start')
                date_range_end = et.SubElement(date_range, 'end')
                date_range_start.text = date_val.split(':')[0]
                date_range_end.text = date_val.split(':')[1]
            else:
                date = et.SubElement(resource, 'date')
                date.text = date_val

            date_prop.getparent().remove(date_prop)
        
        
        '''
        resolve <geoname-prop>
        '''
        for geoname_prop in resource.iter('geoname-prop'):

            geoname = et.SubElement(resource, 'geoname')

            geoname_id = geoname_prop[0].text
            geoname_data = sext.resolve_geonames(geoname_id, project_dir)
            geoname.text = geoname_data['label']
            geoname.set('gnid', 'http://sws.geonames.org/'+geoname_data['name'].split(':')[1])

            if geoname_data.get('lng') and geoname_data.get('lat'):
                geoname.set('lng', geoname_data.get('lng'))
                geoname.set('lat', geoname_data.get('lat'))

            geoname_prop.getparent().remove(geoname_prop)

    # write our new and beautiful XML file
    salsah_export.write(xml_resolved_path, pretty_print=True)

    # write to csv if wished for
    if args.csv:
        parser = et.XMLParser(remove_blank_text=True)
        resolved_xml = et.parse(xml_resolved_path, parser)
        root = resolved_xml.getroot();

        csv_headers = ['id', 'label']
        csv_properties = []
        csv_data = []

        for resource in root.iter('resource'):

            row = {}

            row['id'] = resource.get('id')
            row['label'] = resource.get('label')

            for prop in resource.getchildren():

                tag = prop.tag

                # write tag into csv headers
                if not tag in csv_properties:
                    csv_properties.append(tag)

                if prop.get('comment'):
                    # write comment into csv headers
                    prop_comment = tag+'/comment'
                    if not prop_comment in csv_properties:
                        csv_properties.append(prop_comment)
                    
                    row[prop_comment] = prop.get('comment')

                if prop.get('gnid'):
                    # write comment into csv headers
                    if not 'gnid' in csv_properties:
                        csv_properties.append('gnid')
                    
                    row['gnid'] = prop.get('gnid')

                if prop.get('lat'):
                    # write comment into csv headers
                    if not 'lat' in csv_properties:
                        csv_properties.append('lat')
                    
                    row['lat'] = prop.get('lat')

                if prop.get('lng'):
                    # write comment into csv headers
                    if not 'lng' in csv_properties:
                        csv_properties.append('lng')
                    
                    row['lng'] = prop.get('lng')

                if len(prop.getchildren()) > 1:
                    for idx, sub_prop in enumerate(prop.getchildren()):
                        # write sub_prop into csv headers
                        sub_prop_tag = tag + '_' + str(idx)
                        if not sub_prop_tag in csv_properties:
                            csv_properties.append(sub_prop_tag)

                        if len(sub_prop.keys()):
                            row[sub_prop_tag] = sub_prop.get(sub_prop.keys()[0])
                        else:
                            row[sub_prop_tag] = sub_prop.text
                else:
                    row[tag] = prop.text
            
            csv_data.append(row)

        csv_headers.extend(sorted(csv_properties))
        csv_headers.remove('daterange')

        with open(csv_path, mode='w') as csv_file:
            fieldnames = csv_headers
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            for row in csv_data:
                writer.writerow(row)

if __name__ == '__main__':
    main()
