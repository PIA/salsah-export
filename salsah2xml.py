import argparse
import json
import os
import shutil
from enum import Enum
from pprint import pprint
from typing import Dict

import jdcal
import magic
import requests
from lxml import etree


# MySQL access to old salsah
#
# https://phpmyadmin.sw-zh-dasch-prod-02.prod.dasch.swiss
#

class AdminRights(Enum):
    ADMIN_PROPERTIES = 1  # may add/modify new properties within vocabularies belonging to the project
    ADMIN_RESOURCE_TYPES = 2  # may add/modify new resource_types within vocabularies belonging to the project
    ADMIN_RIGHTS = 4  # ???
    ADMIN_PERSONS = 8  # may add/modify persons within the project
    ADMIN_ADD_RESOURCE = 256  # may add/upload a new resource belonging to project or to system (project_id = 0)
    ADMIN_ROOT = 65536  # = 2^16


class ResourceRights(Enum):
    RESOURCE_ACCESS_NONE = 0  # Resource is not visible
    RESOURCE_ACCESS_VIEW_RESTRICTED = 1  # Resource is viewable with restricted rights (e.g. watermark)
    RESOURCE_ACCESS_VIEW = 2  # Resource is viewable, potentially with properties
    RESOURCE_ACCESS_ANNOTATE = 3  # User may add annotation properties/values
    RESOURCE_ACCESS_EXTEND = 4  # User may add a new value to properties which allow it according to the occurence values
    RESOURCE_ACCESS_OVERRIDE = 5  # User may break the rules and add non-standard properties
    RESOURCE_ACCESS_MODIFY = 6  # User may modify the resource, it's location and all it's associated properties,
    RESOURCE_ACCESS_DELETE = 7  # User may delete the resource and it's associated properties
    RESOURCE_ACCESS_RIGHTS = 8  # User may change the access rights


class ValueRights(Enum):
    VALUE_ACCESS_NONE = 0  # Value is not visible
    VALUE_ACCESS_VIEW = 1  # Value is visible, but may not be changed
    VALUE_ACCESS_ANNOTATE = 2  # Value is visible, but may not be changed but annotated
    VALUE_ACCESS_MODIFY = 3  # Value can be modified


Valtype = {
    '1': 'text',
    '2': 'integer',
    '3': 'decimal',
    '4': 'date',
    '5': 'period',
    '6': 'resptr',
    '7': 'selection',
    '8': 'time',
    '9': 'interval',
    '10': 'geometry',
    '11': 'color',
    '12': 'hlist',
    '13': 'iconclass',
    '14': 'richtext',
    '15': 'geoname'
}


class ValtypeMap(Enum):
    TEXT = 1
    INTEGER = 2
    FLOAT = 3
    DATE = 4
    PERIOD = 5
    RESPTR = 6
    SELECTION = 7
    TIME = 8
    INTERVAL = 9
    GEOMETRY = 10
    COLOR = 11
    HLIST = 12
    ICONCLASS = 13
    RICHTEXT = 14
    GEONAME = 15


stags = {
    '_link': ['<a href="{}">', '<a class="salsah-link" href="IRI:{}:IRI">'],
    'bold': '<strong>',
    'strong': '<strong>',
    'underline': '<u>',
    'italic': '<em>',
    'linebreak': '<br/>',
    'strikethrough': '<strike>',
    'style': '<span gaga={}>',
    'ol': '<ol>',
    'ul': '<ul>',
    'li': '<li>',
    'sup': '<sup>',
    'sub': '<sub>',
    'p': '<p>',
    'h1': '<h1>',
    'h2': '<h2>',
    'h3': '<h3>',
    'h4': '<h4>',
    'h5': '<h5>',
    'h6': '<h6>',
}

etags = {
    '_link': '</a>',
    'bold': '</strong>',
    'strong': '</strong>',
    'underline': '</u>',
    'italic': '</em>',
    'linebreak': '',
    'strikethrough': '<strike>',
    'style': '</span',
    'ol': '</ol>',
    'ul': '</ul>',
    'li': '</li>',
    'sup': '</sup>',
    'sub': '</sub>',
    'p': '</p>',
    'h1': '</h1>',
    'h2': '</h2>',
    'h3': '</h3>',
    'h4': '</h4>',
    'h5': '</h5>',
    'h6': '</h6>',
}


def process_richtext(utf8str: str, textattr: str, resptrs: list) -> (str, str):
    if textattr is not None:
        try:
            attributes = json.loads(textattr)
        except ValueError:
            return 'utf8', utf8str
        
        if len(attributes) == 0:
            return 'utf8', utf8str
        attrlist = []
        result = ''
        for key, vals in attributes.items():
            for val in vals:
                attr = {'tagname': key, 'type': 'start', 'pos': int(val['start'])}
                if val.get('href'):
                    attr['href'] = val['href']
                if val.get('resid'):
                    attr['resid'] = val['resid']
                if val.get('style'):
                    attr['style'] = val['style']
                attrlist.append(attr)
                attr = {'tagname': key, 'type': 'end', 'pos': val['end']}
                attrlist.append(attr)
        attrlist = sorted(attrlist, key=lambda a: a['pos'])
        pos: int = 0
        stack = []
        for attr in attrlist:
            result += utf8str[pos:attr['pos']]
            if attr['type'] == 'start':
                if attr['tagname'] == '_link':
                    if attr.get('resid') is not None:
                        result += stags[attr['tagname']][1].format(attr['resid'])
                    else:
                        result += stags[attr['tagname']][0].format(attr['href'])
                else:
                    result += stags[attr['tagname']]
                stack.append(attr)
            elif attr['type'] == 'end':
                match = False
                tmpstack = []
                while True:
                    tmp = stack.pop()
                    result += etags[tmp['tagname']]
                    if tmp['tagname'] == attr['tagname'] and tmp['type'] == 'start':
                        match = True
                        break
                    else:
                        tmpstack.append(tmp)
                while len(tmpstack) > 0:
                    tmp = tmpstack.pop()
                    if isinstance(stags[tmp['tagname']], str):
                        result += stags[tmp['tagname']]
                    stack.append(tmp)
            pos = attr['pos']
        return 'xml', result
    else:
        return 'utf8', utf8str


class Richtext:

    def __init__(self) -> None:
        super().__init__()


class SalsahError(Exception):
    """Handles errors happening in this file"""

    def __init__(self, message: str) -> None:
        self.message = message


class Salsah:
    def __init__(
            self,
            server: str,
            user: str,
            password: str,
            filename: str,
            assets_path: str,
            projectname: str,
            shortcode: str,
            resptrs: dict,
            session: requests.Session) -> None:
        """

        :param server: Server of old SALSAH (local or https://salsah.org)
        :param user: User for login to old SALSAH server
        :param password: Password for login to old SALSAH server
        :param filename:
        :param projectname: Name of the project to dump
        :param shortcode: Shortcode for Knora that is reserved for the project
        :param resptrs: XML file  containing  object information for resource pointer
        :param session: Session object
        """
        super().__init__()
        self.server: str = server
        self.user: str = user
        self.password: str = password
        self.filename: str = filename
        self.assets_path: str = assets_path
        self.projectname: str = projectname
        self.shortcode: str = shortcode
        self.resptrs: dict = resptrs
        self.session: requests.Session = session

        self.selection_mapping: Dict[str, str] = {}
        self.selection_node_mapping: Dict[str, str] = {}
        self.hlist_mapping: Dict[str, str] = {}
        self.hlist_node_mapping: Dict[str, str] = {}
        self.vocabulary: str = ""

        xsi_namespace = "https://www.w3.org/2001/XMLSchema-instance"
        nsmap = {
            'xsi': xsi_namespace,
        }
        attr_qname = etree.QName("https://www.w3.org/2001/XMLSchema-instance", "schemaLocation")
        self.root = etree.Element('knora', nsmap=nsmap)
        self.root.set(attr_qname, "../knora-data-schema.xsd")
        self.root.set('shortcode', self.shortcode)
        self.mime = magic.Magic(mime=True)
        self.session = session

    def get_icon(self, iconsrc: str, name: str) -> str:
        """
        Get an icon from old SALSAH
        :param iconsrc: URL for icon in old SALSAH
        :param name: nameof the icon
        :return: Path to the icon on local disk
        """
        iconpath: str = os.path.join(self.assets_path, name)
        dlfile = self.session.get(iconsrc, stream=True)  # war urlretrieve()
        with open(iconpath, 'w+b') as fd:
            for chunk in dlfile.iter_content(chunk_size=128):
                fd.write(chunk)
            fd.close()

        mimetype: str = self.mime.from_file(iconpath)
        ext: str
        if mimetype == 'image/gif':
            ext = '.gif'
        elif mimetype == 'image/png':
            ext = '.png'
        elif mimetype == 'image/svg+xml':
            ext = '.svg'
        elif mimetype == 'image/jpeg':
            ext = '.jpg'
        elif mimetype == 'image/jp2':
            ext = '.jp2'    
        elif mimetype == 'image/tiff':
            ext = '.tif'
        else:
            ext = '.img'
        os.rename(iconpath, iconpath + ext)
        return iconpath + ext

    def get_project(self) -> dict:
        """
        Get project info
        :return: Project information that can be dumped as json for knora-create-ontology"
        """
        #
        # first get all system ontologies
        #
        req = self.session.get(self.server + '/api/vocabularies/0?lang=all', auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])
        sys_vocabularies = result['vocabularies']

        #
        # get project info
        #
        req = self.session.get(self.server + '/api/projects/' + self.projectname + "?lang=all",
                               auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])

        project_container = {
            "prefixes": dict(map(lambda a: (a['shortname'], a['uri']), sys_vocabularies)),
            "project": {
                'shortcode': self.shortcode,
                'shortname': result['project_info']['shortname'],
                'longname': result['project_info']['longname'],
            },
        }
        project_info = result['project_info']  # Is this the project_container??? Decide later
        project = {
            'shortcode': self.shortcode,
            'shortname': project_info['shortname'],
            'longname': project_info['longname'],
            'descriptions': dict(map(lambda a: (a['shortname'], a['description']), project_info['description'])),
            'users': [{
                "username": "testuser",
                "email": "testuser@test.org",
                "givenName": "test",
                "familyName": "user",
                "password": "test",
                "lang": "en"
            }]
        }
        if project_info['keywords'] is not None:
            project['keywords'] = list(map(lambda a: a.strip(), project_info['keywords'].split(',')))
        else:
            project['keywords'] = [result['project_info']['shortname']]

        #
        # Get the vocabulary. The old Salsah uses only one vocabulary per project....
        # Note: the API call always returns also the system vocabularies which we have to be excluded
        #
        req = self.session.get(self.server + '/api/vocabularies/' + self.projectname, auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])
        vocabulary = None
        for voc in result['vocabularies']:
            if int(voc['project_id']) != 0:
                vocabulary = voc

        self.vocabulary = vocabulary['shortname']
        project['lists'] = self.get_selections_of_vocabulary(vocabulary['shortname'])
        self.root.set('default-ontology', vocabulary['shortname'])

        project['ontology'] = {
            'name': vocabulary['shortname'],
            'label': vocabulary['longname'],
        }
        #
        # ToDo: not yet implemented in create_ontology
        # if vocabulary.get('description') is not None and vocabulary['description']:
        #    project['ontology']['comment'] = vocabulary['description']

        project['ontology']['resources'] = self.get_resourcetypes_of_vocabulary(vocabulary['shortname'])
        project_container["project"] = project
        return project_container

    def get_properties_of_resourcetype(self, vocname: str, restype_id: int, salsah_restype_info: dict) -> list:

        gui_attr_lut = {
            'text': ['size', 'maxlength'],
            'textarea': ['width', 'rows', 'wrap'],
            'pulldown': ['selection'],
            'slider': ['stepsize'],
            'searchbox': ['numprops'],
            'colorpicker': ['ncolors'],
            'hlist': ['hlist'],
            'radio': ['selection'],
            'interval': ['duration']
        }
        props = []
        for properties in salsah_restype_info[restype_id]['properties']:
            if properties['name'] == '__location__':
                continue
            if properties['name'].startswith('has'):
                pname = properties['name']
            else:
                pname = 'has' + properties['name'].capitalize()
            prop = {
                'name': pname,
                'labels': dict(map(lambda a: (a['shortname'], a['label']), properties['label'])),
            }
            if properties.get('description') is not None:
                prop['comments']: Dict[map(lambda a: (a['shortname'], a['label']), properties['description'])]

            #
            # convert attributes into dict
            #
            attrdict = {}
            if properties.get('attributes') is not None:
                attributes = properties['attributes'].split(';')
                for attribute in attributes:
                    if attribute:
                        (key, val) = attribute.split('=')
                        attrdict[key] = val

            if properties['vocabulary'] == 'salsah':
                if properties['name'] == 'color':
                    knora_super = ['hasColor']
                    knora_object = 'ColorValue'
                elif properties['name'] == 'comment' or properties['name'] == 'comment_rt':
                    knora_super = ['hasComment']
                    knora_object = 'TextValue'
                elif properties['name'] == 'external_id':
                    # ToDo: implement external_id
                    raise SalsahError("SALSAH-ERROR:\n\"external_id\" NYI!!")
                elif properties['name'] == 'external_provider':
                    # ToDo: implement external_provider
                    raise SalsahError("SALSAH-ERROR:\n\"external_provider\" NYI!!")
                elif properties['name'] == 'geometry':
                    knora_super = ['hasGeometry']
                    knora_object = 'GeomValue'
                elif properties['name'] == 'part_of':
                    knora_super = ['isPartOf']
                    if self.resptrs.get(salsah_restype_info[restype_id]['name']) is not None:
                        tmp = self.resptrs[salsah_restype_info[restype_id]['name']]
                        if tmp.get('salsah:part_of') is not None:
                            knora_object = tmp['salsah:part_of']
                    else:
                        knora_object = 'FIXME--Resource--FIXME'
                        print("WARNING: Resclass {} has resptr {} with no object!".format(
                            salsah_restype_info[restype_id]['name'], properties['name']))
                elif properties['name'] == 'region_of':
                    knora_super = ['isRegionOf']
                    if self.resptrs.get(salsah_restype_info[restype_id]['name']) is not None:
                        tmp = self.resptrs[salsah_restype_info[restype_id]['name']]
                        if tmp.get('salsah:region_of') is not None:
                            knora_object = tmp['salsah:part_of']
                    else:
                        knora_object = 'FIXME--Resource--FIXME'
                        print("WARNING: Resclass {} has resptr {} with no object!".format(
                            salsah_restype_info[restype_id]['name'], properties['name']))
                elif properties['name'] == 'resource_reference':
                    knora_super = ['hasLinkTo']
                    if self.resptrs.get(salsah_restype_info[restype_id]['name']) is not None:
                        tmp = self.resptrs[salsah_restype_info[restype_id]['name']]
                        if tmp.get('salsah:resource_reference') is not None:
                            knora_object = tmp['salsah:part_of']
                    else:
                        knora_object = 'FIXME--Resource--FIXME'
                        print("WARNING: Resclass {} has resptr {} with no object!".format(
                            salsah_restype_info[restype_id]['name'], properties['name']))
                elif properties['name'] == 'interval':
                    knora_super = ['hasValue']
                    knora_object = 'IntervalValue'
                elif properties['name'] == 'time':
                    # ToDo: implement TimeValue in knora-base
                    raise SalsahError("SALSAH-ERROR:\n\"TimeValue\" NYI!!")
                elif properties['name'] == 'seqnum':
                    knora_super = ['seqnum']
                    knora_object = 'IntValue'
                elif properties['name'] == 'sequence_of':
                    knora_super = ['isPartOf']
                    if self.resptrs.get(salsah_restype_info[restype_id]['name']) is not None:
                        tmp = self.resptrs[salsah_restype_info[restype_id]['name']]
                        if tmp.get('salsah:resource_reference') is not None:
                            knora_object = tmp['salsah:part_of']
                    else:
                        knora_object = 'FIXME--Resource--FIXME'
                elif properties['name'] == 'uri':
                    knora_super = ['hasValue']
                    knora_object = 'UriValue'
                else:
                    knora_super = ['hasValue']
                    knora_object = 'TextValue'
            elif properties['vocabulary'] == 'dc':
                knora_super = ['hasValue',
                               'dc:' + properties['name'] if properties[
                                                                 'name'] != 'description_rt' else 'dc:description']
                knora_object = 'TextValue'
            elif properties['vocabulary'] == vocname:
                if properties['vt_php_constant'] == 'VALTYPE_TEXT':
                    knora_super = ['hasValue']
                    knora_object = 'TextValue'
                elif properties['vt_php_constant'] == 'VALTYPE_INTEGER':
                    knora_super = ['hasValue']
                    knora_object = 'IntValue'
                elif properties['vt_php_constant'] == 'VALTYPE_FLOAT':
                    knora_super = ['hasValue']
                    knora_object = 'DecimalValue'
                elif properties['vt_php_constant'] == 'VALTYPE_DATE':
                    knora_super = ['hasValue']
                    knora_object = 'DateValue'
                elif properties['vt_php_constant'] == 'VALTYPE_PERIOD':
                    knora_super = ['hasValue']
                    knora_object = 'DateValue'
                elif properties['vt_php_constant'] == 'VALTYPE_RESPTR':
                    knora_super = ['hasLinkTo']
                    knora_object = None
                    if attrdict.get('restypeid') is None:
                        tmp = self.resptrs[salsah_restype_info[restype_id]['name']]
                        if tmp.get('salsah:resource_reference') is not None:
                            knora_object = tmp[properties['default-ontology'] + ':' + properties['name'].capitalize()]
                    else:
                        if salsah_restype_info.get(attrdict['restypeid']) is None:
                            tmp = self.resptrs[salsah_restype_info[restype_id]['name']]
                            if tmp.get('salsah:resource_reference') is not None:
                                knora_object = tmp[properties['vocabulary'] + ':' + properties['name'].capitalize()]
                            raise SalsahError("SALSAH-ERROR:\n\"restypeid\" is missing!")
                        (voc, restype) = salsah_restype_info[attrdict['restypeid']]['name'].split(':')
                        knora_object = voc + ':' + restype.capitalize()
                    if knora_object is None:
                        knora_object = 'FIXME--Resource--FIXME'
                        print("WARNING: Resclass {} has resptr {} with no object!".format(
                            salsah_restype_info[restype_id]['name'], properties['name']))
                elif properties['vt_php_constant'] == 'VALTYPE_SELECTION':
                    knora_super = ['hasValue']
                    knora_object = 'ListValue'
                elif properties['vt_php_constant'] == 'VALTYPE_TIME':
                    knora_super = ['hasValue']
                    knora_object = 'TimeValue'
                elif properties['vt_php_constant'] == 'VALTYPE_INTERVAL':
                    knora_super = ['hasValue']
                    knora_object = 'IntervalValue'
                elif properties['vt_php_constant'] == 'VALTYPE_GEOMETRY':
                    knora_super = ['hasValue']
                    knora_object = 'GeomValue'
                elif properties['vt_php_constant'] == 'VALTYPE_COLOR':
                    knora_super = ['hasValue']
                    knora_object = 'ColorValue'
                elif properties['vt_php_constant'] == 'VALTYPE_HLIST':
                    knora_super = ['hasValue']
                    knora_object = 'ListValue'
                elif properties['vt_php_constant'] == 'VALTYPE_ICONCLASS':
                    knora_super = ['hasValue']
                    knora_object = 'TextValue'
                elif properties['vt_php_constant'] == 'VALTYPE_RICHTEXT':
                    knora_super = ['hasValue']
                    knora_object = 'TextValue'
                elif properties['vt_php_constant'] == 'VALTYPE_GEONAME':
                    knora_super = ['hasValue']
                    knora_object = 'GeonameValue'
                else:
                    raise SalsahError(
                        "SALSAH-ERROR:\n\"Invalid vocabulary used: " + properties['vocabulary'] + " by property " +
                        properties['name'])
            else:
                raise SalsahError(
                    "SALSAH-ERROR:\n\"Invalid vocabulary used: " + properties['vocabulary'] + " by property " +
                    properties['name'])

            gui_attributes = []
            if properties['gui_name'] == 'text':
                gui_element = 'SimpleText'
                for attr in gui_attr_lut['text']:
                    if attrdict.get(attr):
                        gui_attributes.append(attr + '=' + attrdict[attr])
            elif properties['gui_name'] == 'textarea':
                gui_element = 'Textarea'
                for attr in gui_attr_lut['textarea']:
                    if attrdict.get(attr):
                        gui_attributes.append(attr + '=' + attrdict[attr])
            elif properties['gui_name'] == 'pulldown':
                gui_element = 'Pulldown'
                for attr in gui_attr_lut['pulldown']:
                    if attrdict.get(attr) and attr == 'selection':
                        gui_attributes.append('hlist=' + self.selection_mapping[attrdict[attr]])
            elif properties['gui_name'] == 'slider':
                gui_element = 'Slider'
                for attr in gui_attr_lut['slider']:
                    if attrdict.get(attr):
                        gui_attributes.append(attr + '=' + attrdict[attr])
            elif properties['gui_name'] == 'spinbox':
                gui_element = 'Spinbox'
            elif properties['gui_name'] == 'searchbox':
                gui_element = 'Searchbox'
                for attr in gui_attr_lut['searchbox']:
                    if attrdict.get(attr):
                        gui_attributes.append(attr + '=' + attrdict[attr])
            elif properties['gui_name'] == 'date':
                gui_element = 'Date'
            elif properties['gui_name'] == 'geometry':
                gui_element = 'Geometry'
            elif properties['gui_name'] == 'colorpicker':
                gui_element = 'Colorpicker'
                for attr in gui_attr_lut['colorpicker']:
                    if attrdict.get(attr):
                        gui_attributes.append(attr + '=' + attrdict[attr])
            elif properties['gui_name'] == 'hlist':
                gui_element = 'List'
                for attr in gui_attr_lut['hlist']:
                    if attrdict.get(attr) and attr == 'hlist':
                        gui_attributes.append(attr + '=' + self.hlist_mapping[attrdict[attr]])
            elif properties['gui_name'] == 'radio':
                gui_element = 'Radio'
                for attr in gui_attr_lut['pulldown']:
                    if attrdict.get(attr) and attr == 'selection':
                        gui_attributes.append('hlist=' + self.selection_mapping[attrdict[attr]])
            elif properties['gui_name'] == 'richtext':
                gui_element = 'Richtext'
            elif properties['gui_name'] == 'time':
                gui_element = 'Time'
            elif properties['gui_name'] == 'interval':
                gui_element = 'Interval'
                for attr in gui_attr_lut['interval']:
                    if attrdict.get(attr):
                        gui_attributes.append(attr + '=' + attrdict[attr])
            elif properties['gui_name'] == 'geoname':
                gui_element = 'Geonames'
            else:
                raise SalsahError(
                    "SALSAH-ERROR:\n\"Invalid gui_element: " + properties['gui_name'] + " by property " +
                    properties['name'])

            prop['super'] = knora_super
            prop['object'] = knora_object
            prop['gui_element'] = gui_element
            if len(gui_element) > 0:
                prop['gui_attributes'] = gui_attributes
            prop['cardinality'] = properties['occurrence']

            props.append(prop)
        return props

    def get_resourcetypes_of_vocabulary(self, vocname):
        """
        Fetches Ressourcetypes and returns restypes
        """
        payload: dict = {
            'vocabulary': vocname,
            'lang': 'all'
        }
        req = self.session.get(self.server + '/api/resourcetypes/', params=payload, auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])

        restype_ids: list = list(map(lambda r: r['id'], result['resourcetypes']))

        salsah_restype_info: dict = {}
        for restype_id in restype_ids:
            payload: dict = {
                'lang': 'all'
            }
            req = self.session.get(self.server + '/api/resourcetypes/' + restype_id, params=payload,
                                   auth=(self.user, self.password))
            result = req.json()
            if result['status'] != 0:
                raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])
            salsah_restype_info[restype_id] = result['restype_info']

        restypes_container: list = []
        for restype_id in restype_ids:
            restype_info = salsah_restype_info[restype_id]
            (voc, name) = restype_info['name'].split(':')
            if voc != vocname:
                raise SalsahError("SALSAH-ERROR:\nresourcetype from other vocabulary! " + restype_info['name'])
            super = ''
            if restype_info['class'] == 'object':
                super = 'Resource'
            elif restype_info['class'] == 'image':
                super = 'StillImageRepresentation'
            elif restype_info['class'] == 'movie':
                super = 'MovingImageRepresentation'
            else:
                raise SalsahError("SALSAH-ERROR:\nResource class not supported! " + restype_info['name'])

            labels = dict(map(lambda a: (a['shortname'], a['label']), restype_info['label']))

            group_switcher = {
                1: 'UnknownUser',
                2: 'KnownUser',
                3: 'ProjectMember',
                4: 'Creator'
            }
            rights_switcher = {
                ResourceRights.RESOURCE_ACCESS_NONE.value: 'NONE',
                ResourceRights.RESOURCE_ACCESS_VIEW_RESTRICTED.value: 'RV',
                ResourceRights.RESOURCE_ACCESS_VIEW.value: 'V',
                ResourceRights.RESOURCE_ACCESS_ANNOTATE.value: 'V',
                ResourceRights.RESOURCE_ACCESS_EXTEND.value: 'V',
                ResourceRights.RESOURCE_ACCESS_OVERRIDE.value: 'V',
                ResourceRights.RESOURCE_ACCESS_MODIFY.value: 'M',
                ResourceRights.RESOURCE_ACCESS_DELETE.value: 'D',
                ResourceRights.RESOURCE_ACCESS_RIGHTS.value: 'CR'
            }

            default_rights = {}
            if restype_info.get('default_rights') is not None:
                for rights in restype_info['default_rights']:
                    print('group_id:', rights['group_id'])
                    print('default_rights', rights['default_rights'])
                    default_rights[group_switcher[int(rights['group_id'])]] = rights_switcher[
                        int(rights['default_rights'])]
                restype = {
                    'name': name.capitalize(),
                    'super': super,
                    'labels': labels,
                    'default_rights': default_rights
                }
                permission = etree.Element('permissions', {
                    'id': 'defperm_' + name.capitalize(),
                })
            else:
                permission = etree.Element('permissions', {
                    'id': '__NONE__',
                })
            for g, p in default_rights.items():
                allow = etree.Element('allow', {'group': g})
                allow.text = p
                permission.append(allow)
            self.root.append(permission)

            restype = {}
            if restype_info.get('description') is not None:
                comments = dict(map(lambda a: (a['shortname'], a['description']), restype_info['description']))
                restype['comments'] = comments

            # if restype_info.get('iconsrc') is not None:
            #     restype['iconsrc'] = self.get_icon(restype_info['iconsrc'], restype_info['name'])

            restype['properties'] = self.get_properties_of_resourcetype(vocname, restype_id, salsah_restype_info)

            restypes_container.append(restype)
        return restypes_container

    def get_selections_of_vocabulary(self, vocname: str):
        """
        Get the selections and hlists. In knora, there are only herarchical lists! A selection is
        just a hierarchical list without children...

        :param vocname: Vocabulary name
        :return: Python list of salsah selections and hlists as knora lists
        """
        #
        # first we get the flat lists (selctions)
        #
        payload = {
            'vocabulary': vocname,
            'lang': 'all'
        }
        req = self.session.get(self.server + '/api/selections', params=payload, auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])

        selections = result['selections']

        # Let's make an empty list for the lists:
        selections_container = []

        for selection in selections:
            self.selection_mapping[selection['id']] = selection['name']
            if isinstance(selection['name'], dict):
                root = {
                    'name': selection['name'],
                    'labels': dict(map(lambda a: (a['shortname'], a['label']), selection['label']))
                }
            else:
                root = {
                    'name': selection['name'],
                    'labels': {'de': a for a in selection['label']}
                }
            if selection.get('description') is not None:
                root['comments'] = dict(map(lambda a: (a['shortname'], a['description']), selection['description']))
            payload = {'lang': 'all'}
            req_nodes = self.session.get(self.server + '/api/selections/' + selection['id'], params=payload,
                                         auth=(self.user, self.password))
            result_nodes = req_nodes.json()
            if result_nodes['status'] != 0:
                raise SalsahError("SALSAH-ERROR:\n" + result_nodes['errormsg'])
            self.selection_node_mapping.update(dict(map(lambda a: (a['id'], a['name']), result_nodes['selection'])))
            root['nodes'] = list(map(lambda a: {
                'name': 'S_' + a['id'],
                'labels': a['label']
            }, result_nodes['selection']))
            selections_container.append(root)

        #
        # now we get the hierarchical lists (hlists)
        #
        payload = {
            'vocabulary': vocname,
            'lang': 'all'
        }
        req = self.session.get(self.server + '/api/hlists', params=payload, auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])
        self.hlist_node_mapping.update(dict(map(lambda a: (a['id'], a['name']), result['hlists'])))

        hlists = result['hlists']

        #
        # this is a helper function for easy recursion
        #
        def process_children(children: list) -> list:
            newnodes = []
            for node in children:
                self.hlist_node_mapping[node['id']] = node['name']
                newnode = {
                    'name': 'H_' + node['id'],
                    'labels': dict(map(lambda a: (a['shortname'], a['label']), node['label']))
                }
                if node.get('children') is not None:
                    newnode['nodes'] = process_children(node['children'])
                newnodes.append(newnode)
            return newnodes

        for hlist in hlists:
            root = {
                'name': hlist['name'],
                'labels': dict(map(lambda a: (a['shortname'], a['label']), hlist['label']))
            }
            self.hlist_mapping[hlist['id']] = hlist['name']
            if hlist.get('description') is not None:
                root['comments'] = dict(map(lambda a: (a['shortname'], a['description']), hlist['description']))
            payload = {'lang': 'all'}
            req_nodes = self.session.get(self.server + '/api/hlists/' + hlist['id'], params=payload,
                                         auth=(self.user, self.password))
            result_nodes = req_nodes.json()
            if result_nodes['status'] != 0:
                raise SalsahError("SALSAH-ERROR:\n" + result_nodes['errormsg'])
            root['nodes'] = process_children(result_nodes['hlist'])
            selections_container.append(root)

        return selections_container

    def get_all_obj_ids(self, project: str, restype: str = 'ALL', pfilter: str = "", start_at: int = 0,
                        show_nrows: int = -1):
        """
        Get all resource id's from project
        :param project: Project name
        :param restype: Resource type name, "ALL" for all
        :param pfilter: Gaga ToDo: document
        :param start_at: Start at given resource
        :param show_nrows: Show n resources
        :return:
        """
        payload = {
            'searchtype': 'extended',
            'filter_by_project': project
        }
        if restype != 'ALL':
            payload['filter_by_restype'] = restype
        if pfilter != "":
            # <voc:propertyname>=<value>
            prop, val = pfilter.split('=')
            payload['property_id'] = prop
            payload['compop'] = 'EQ'
            payload['searchval'] = val
        if show_nrows > 0:
            payload['show_nrows'] = show_nrows
            payload['start_at'] = start_at

        req = self.session.get(self.server + '/api/search/', params=payload, auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])
        else:
            nhits = result['nhits']
            obj_ids = [entry['obj_id'] for entry in result['subjects']]
            return nhits, obj_ids

    def get_resource(self, res_id: str) -> Dict:
        payload = {
            'reqtype': 'info'
        }
        req = self.session.get(self.server + '/api/resources/' + res_id, params=payload,
                               auth=(self.user, self.password))
        result = req.json()

        if result['status'] != 0:
            # raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])
            return {}
        firstproperty = result['resource_info']['firstproperty']

        req = self.session.get(self.server + '/api/resources/' + res_id, auth=(self.user, self.password))
        result = req.json()
        if result['status'] != 0:
            # raise SalsahError("SALSAH-ERROR:\n" + result['errormsg'])
            return {}
        result['firstproperty'] = firstproperty
        return result

    def write_json(self, proj: Dict):
        """
        We send the dict to JSON and write it to the project-folder
        """
        json_filename = self.filename + '.json'
        pro_cont = json.dumps(proj, indent=4)
        with open(json_filename, "w") as f:
            f.write(pro_cont)

    def process_value(self, valtype: int, value: any, verbose: bool, comment: str = None):
        valele = None
        if valtype == ValtypeMap.TEXT.value:
            if value:
                valele = etree.Element('text')
                valele.text = value
                valele.set('encoding', 'utf8')
        elif valtype == ValtypeMap.RICHTEXT.value:
            if value.get('utf8str').strip():
                valele = etree.Element('text')
                resptrs = value['resource_reference']
                encoding, valele.text = process_richtext(
                    utf8str=value.get('utf8str').strip(),
                    textattr=value.get('textattr').strip(),
                    resptrs=value.get('resptrs'))
                resrefs = '|'.join(value['resource_reference'])
                if len(resrefs) > 0:
                    valele.set('resrefs', resrefs)
                valele.set('encoding', encoding)
        elif valtype == ValtypeMap.COLOR.value:
            if value:
                valele = etree.Element('color')
                valele.text = value
        elif valtype == ValtypeMap.DATE.value:
            if value:
                valele = etree.Element('date')
                cal: str
                if value['calendar'] == 'GREGORIAN':
                    cal = 'GREGORIAN'
                    start = jdcal.jd2gcal(int(value['dateval1']), 0)
                    end = jdcal.jd2gcal(int(value['dateval2']), 0)
                else:
                    cal = 'JULIAN'
                    start = jdcal.jd2jcal(int(value['dateval1']), 0)
                    end = jdcal.jd2jcal(int(value['dateval2']), 0)
                p1: str = 'CE'
                if start[0] <= 0:
                    start[0] = start[0] - 1
                    p1 = 'BCE'

                p2: str = 'CE'
                if end[0] <= 0:
                    end[0] = end[0] - 1
                    p2 = 'BCE'

                endstr: str = ""

                if value['dateprecision1'] == 'YEAR':
                    startstr = "{}:{}:{:04d}".format(cal, p1, start[0])
                elif value['dateprecision1'] == 'MONTH':
                    startstr = "{}:{}:{:04d}-{:02d}".format(cal, p1, start[0], start[1])
                else:
                    startstr = "{}:{}:{:04d}-{:02d}-{:02d}".format(cal, p1, start[0], start[1], start[2])

                if value['dateprecision2'] == 'YEAR':
                    if start[0] != end[0]:
                        endstr = ":{}:{:04d}".format(p2, end[0])
                elif value['dateprecision2'] == 'MONTH':
                    if start[0] != end[0] or start[1] != end[1]:
                        endstr = ":{}:{:04d}-{:02d}".format(p2, end[0], end[1])
                else:
                    if start[0] != end[0] or start[1] != end[1] or start[2] != end[2]:
                        endstr = ":{}:{:04d}-{:02d}-{:02d}".format(p2, end[0], end[1], end[2])
                valele.text = startstr + endstr
        elif valtype == ValtypeMap.FLOAT.value:
            if value:
                valele = etree.Element('decimal')
                valele.text = value
        elif valtype == ValtypeMap.GEOMETRY.value:
            if value:
                valele = etree.Element('geometry')
                valele.text = value
        elif valtype == ValtypeMap.GEONAME.value:
            if value:
                valele = etree.Element('geoname')
                valele.text = value
        elif valtype == ValtypeMap.HLIST.value:
            if value:
                valele = etree.Element('list')
                valele.text = 'H_' + value
        elif valtype == ValtypeMap.ICONCLASS.value:
            if value:
                valele = etree.Element('iconclass')
                valele.text = value
        elif valtype == ValtypeMap.INTEGER.value:
            if value:
                valele = etree.Element('integer')
                valele.text = value
        elif valtype == ValtypeMap.INTERVAL.value:
            if value:
                valele = etree.Element('interval')
                valele.text = value
        elif valtype == ValtypeMap.PERIOD.value:
            valele = etree.Element('period')
            pass
        elif valtype == ValtypeMap.RESPTR.value:
            if value:
                valele = etree.Element('resptr')
                valele.text = value
        elif valtype == ValtypeMap.SELECTION.value:
            if value:
                valele = etree.Element('list')
                valele.text = 'S_' + value
        elif valtype == ValtypeMap.TIME.value:
            if value:
                if isinstance(value, dict):
                    valele = etree.Element('interval')
                    valele.text = value['timeval1'] + ':' + value['timeval2']
                else:
                    valele = etree.Element('time')
                    valele.text = value
        else:
            print('===========================')
            pprint(value)
            print('----------------------------')
        if comment is not None:
            if verbose:
                print('Comment: ' + comment)
            valele.set('comment', comment)
        return valele  # Das geht in die Resourcen

    def process_property(self, propname: str, prop: Dict, verbose: bool):
        if propname == '__location__':
            return None
        if prop.get("values") is not None:
            #
            # first we strip the vocabulary off, if it's not salsah, dc, etc.
            #
            tmp = propname.split(':')
            if tmp[0] == self.vocabulary or tmp[0] == 'dc':
                propname_new = tmp[1]  # strip vocabulary
                #
                # if the propname does not start with "has", add  it to the propname. We have to do this
                # to avoid naming conflicts between resources and properties which share the same
                # namespace in GraphDB
                #
                if not propname_new.startswith('has'):
                    propname_new = ':has' + propname_new.capitalize()
            else:
                propname_new = ':' + propname
            options: Dict[str, str] = {
                'name': propname_new
            }
            if int(prop["valuetype_id"]) == ValtypeMap.SELECTION.value:
                (dummy, list_id) = prop['attributes'].split("=")
                options['list'] = self.selection_mapping[list_id]
            elif int(prop["valuetype_id"]) == ValtypeMap.HLIST.value:
                (dummy, list_id) = prop['attributes'].split("=")
                options['list'] = self.hlist_mapping[list_id]

            if Valtype.get(prop["valuetype_id"]) == 'richtext':
                pname = 'text-prop'
            elif Valtype.get(prop["valuetype_id"]) == 'hlist':
                pname = 'list-prop'
            elif Valtype.get(prop["valuetype_id"]) == 'selection':
                pname = 'list-prop'
            else:
                pname = Valtype.get(prop["valuetype_id"]) + '-prop'
            propnode = etree.Element(pname, options)
            cnt: int = 0
            for value in prop["values"]:
                if prop['comments'][cnt]:
                    valnode = self.process_value(int(prop["valuetype_id"]), value, verbose, prop['comments'][cnt])
                    if valnode is not None:
                        propnode.append(valnode)
                        cnt += 1
                else:
                    valnode = self.process_value(int(prop["valuetype_id"]), value, verbose)
                    if valnode is not None:
                        propnode.append(valnode)
                        cnt += 1
            if cnt > 0:
                return propnode
            else:
                return None
        else:
            return None

    def process_resource(self, resource: Dict, images_path: str, download: bool, image_type: str, write_metadata: bool,
                         verbose: bool):
        if not resource:
            return
        tmp = resource["resdata"]["restype_name"].split(':')
        if tmp[0] == self.vocabulary:
            restype = tmp[1].capitalize()
        else:
            restype = resource["resdata"]["restype_name"]
        resnode = etree.Element('resource', {
            'restype': ':' + restype,
            'id': resource["resdata"]["res_id"],
            'label': resource['firstproperty'],
            'permissions': 'defperm_' + restype
        })
        if resource["resinfo"].get('locdata') is not None:
            resource_name = resource["resinfo"]['locdata']['origname']
            if image_type is not None:
                resource_name = os.path.splitext(resource_name)[0] + f'.{image_type}'
            imgpath = os.path.join(images_path, resource_name)
            ext = os.path.splitext(resource_name)[1][1:].strip().lower()
            if ext == 'jpg' or ext == 'jpeg':
                image_format = 'jpg'
            elif ext == 'png':
                image_format = 'png'
            elif ext == 'jp2' or ext == 'jpx':
                image_format = 'jpx'
            else:
                image_format = 'tif'
            getter = resource["resinfo"]['locdata']['path'] + '&format=' + image_format
            if download:
                if verbose:
                    print(f'Downloading {resource_name}...')
                dlfile2 = self.session.get(getter, stream=True)  # war urlretrieve()

                with open(imgpath, 'w+b') as fd:
                    for chunk in dlfile2.iter_content(chunk_size=128):
                        fd.write(chunk)
                    fd.close()

            image_node = etree.Element('image')
            image_node.text = imgpath
            resnode.append(image_node)

        if write_metadata:
            for propname in resource["props"]:
                propnode = self.process_property(propname, resource["props"][propname], verbose)  # process_property()
                if propnode is not None:
                    resnode.append(propnode)
            self.root.append(resnode)  # Das geht in die Resourcen
            if verbose:
                print('Resource added. Id=' + resource["resdata"]["res_id"], flush=True)

    def write_xml(self):
        xml_filename = self.filename + '.xml'
        with open(xml_filename, "wb") as f:
            f.write(etree.tostring(self.root, pretty_print=True, xml_declaration=True, encoding='utf-8'))


def program():
    parser = argparse.ArgumentParser()
    parser.add_argument("user", help="username for SALSAH")
    parser.add_argument("password", help="the password for login")
    parser.add_argument("server", help="URL of the SALSAH server")
    parser.add_argument("-P", "--project", help="shortname or ID of project")
    parser.add_argument("-s", "--shortcode", default='XXXX', help="Knora-shortcode of project")
    parser.add_argument("-R", "--restype", default='ALL', help="resource Type as '<voc:restypename>")
    parser.add_argument("-f", "--filter", default="", help="supposedly a filter by resource type according to one of the Salsah ontologies")
    parser.add_argument("-n", "--nrows", type=int, help="number of records to get, -1 to get all")
    parser.add_argument("-S", "--start", type=int, help="start at record with given number")
    parser.add_argument("-F", "--folder", default="-", help="output folder")
    parser.add_argument("-r", "--resptrs_file", help="list of resptrs targets")
    parser.add_argument("-b", "--batch-size", type=int, default=1000, help="batch size to split download into.")
    parser.add_argument("-t", "--request-image-type", help="request a specified image format.",
                        choices=['jpg', 'png', 'jp2', 'tif'])
    parser.add_argument("-d", "--download", action="store_true", help="download image files.")
    parser.add_argument("-w", "--write-metadata", action="store_true", help="write metadata to files.")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose feedback")
    parser.add_argument("--only-print-ids", action="store_true", help="only print the IDs matching the given request.")

    args = parser.parse_args()

    #
    # here we fetch the shortcodes from the github repository
    #
    shortcode = None
    if args.shortcode == "XXXX":
        r = requests.get(
            'https://raw.githubusercontent.com/dhlab-basel/dasch-ark-resolver-data/master/data/shortcodes.csv')
        lines = r.text.split('\r\n')
        for line in lines:
            parts = line.split(',')
            if len(parts) > 1 and parts[1] == args.project:
                shortcode = parts[0]
                print('Found Knora project shortcode "{}" for "{}"!'.format(shortcode, parts[1]))
    else:
        shortcode = args.shortcode

    if shortcode is None:
        print("You must give a shortcode (\"--shortcode XXXX\")!")
        exit(1)

    user = args.user
    password = args.password
    start = args.start
    nrows = -1 if args.nrows is None else args.nrows
    project = args.project

    verbose = args.verbose
    download = args.download
    write_metadata = args.write_metadata

    resptrs: dict = {}
    if args.resptrs_file is not None:
        tree = etree.parse(args.resptrs_file)
        root = tree.getroot()
        for restype in root:
            restype_name = restype.attrib["name"].strip()
            props: dict = {}
            for prop in restype:
                props[prop.attrib["name"]] = prop.text.strip()
            resptrs[restype.attrib["name"]] = props

    if args.folder == '-':
        folder = args.project + ".dir"
    else:
        folder = args.folder

    assets_path = os.path.join(folder, 'assets')
    images_path = os.path.join(folder, 'images')
    outfile_path = os.path.join(folder, project)

    if download or write_metadata:
        if os.path.exists(folder):
            delete_existing = input('Output directory already exists! Delete existing? [y/N] ')
            if delete_existing.lower() == 'y':
                shutil.rmtree(folder)
            else:
                exit(0)
        try:
            os.mkdir(folder)
            os.mkdir(assets_path)
            os.mkdir(images_path)
        except OSError:
            print("Couldn't create necessary folders")
            exit(2)

    # Define session
    session = requests.Session()

    con = Salsah(server=args.server, user=user, password=password, filename=outfile_path,
                 assets_path=assets_path, projectname=args.project, shortcode=shortcode,
                 resptrs=resptrs, session=session)
    proj = con.get_project()

    if write_metadata:
        con.write_json(proj)

    # Loop until all requested resources have been downloaded
    remaining = nrows
    if nrows == -1:
        remaining = 1000
    total = -1
    batch_size = args.batch_size

    while remaining > 0:
        (nhits, res_ids) = con.get_all_obj_ids(project, args.restype, args.filter, start, min(remaining, batch_size))
        nhits = int(nhits)
        num_results = len(res_ids)
        if args.only_print_ids:
            print(res_ids)
        if verbose:
            if total == -1:
                print(f'Total results = {nhits} (requested {"all" if nrows == -1 else nrows} starting at {start})')
            print(f'Batch size {num_results} [{start}, {start + num_results}]')

        if total == -1:
            total = nhits
            if nrows == -1:
                remaining = nhits
        remaining -= num_results
        start += num_results

        if args.only_print_ids:
            continue

        """
        Write resources to xml
        """
        resources = [con.get_resource(res_id) for res_id in res_ids]

        for resource in resources:
            con.process_resource(resource, images_path, download, args.request_image_type, write_metadata, verbose)

        if write_metadata:
            con.write_xml()


def main():
    program()


if __name__ == '__main__':
    main()
