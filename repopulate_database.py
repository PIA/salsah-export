'''
2021 Participatory Image Archives
Contact: Adrian Demleitner, adrian.demleitner@unibas.ch

https://github.com/Participatory-Image-Archives/salsah-export
'''

import sqlite3, datetime

def main():
    # db connection
    from_conn = sqlite3.connect("/Users/adriandemleitner/Repositories/pia-database/database/pia_production.sqlite")
    from_cur = from_conn.cursor()

    to_conn = sqlite3.connect("/Users/adriandemleitner/Repositories/pia-database/database/pia_repopulated.sqlite")
    to_cur = to_conn.cursor()

    from_cur.execute("Select * from collections where origin = 'pia'")
    collections = from_cur.fetchall()

    # copy collections and their notes  
    for collection in collections:
        to_cur.execute("Insert into collections (created_at, updated_at, deleted_at, label, description, origin, creator) values(?, ?, ?, ?, ?, ?, ?) returning id;", (collection[1], collection[2], collection[3], collection[6], collection[8], collection[11], collection[13]))
        to_collection_id = to_cur.fetchone()[0]

        # copy image relations
        from_cur.execute("Select * from collection_image where collection_id = ?;", [collection[0]])
        image_ids = from_cur.fetchall()
        
        for image_id in image_ids:
            print(image_id)
            to_cur.execute("Insert into collection_image (collection_id, image_id) values (?, ?)", (to_collection_id, image_id[3]))

        # copy notes
        from_cur.execute("Select * from collection_note where collection_id = ?;", [collection[0]])
        note_ids = from_cur.fetchall()

        for note_id in note_ids:
            from_cur.execute("Select * from notes where id = ?;", [note_id[3]])
            note = from_cur.fetchone()

            to_cur.execute("Insert into notes (created_at, updated_at, deleted_at, label, description, content) values(?, ?, ?, ?, ?, ?) returning id;", (note[1], note[2], note[3], note[4], note[5], note[6]))
            to_note_id = to_cur.fetchone()[0]

            to_cur.execute("Insert into collection_note (collection_id, note_id) values (?, ?)", (to_collection_id, to_note_id))
        
    to_conn.commit()
    to_cur.close()

if __name__ == '__main__':
    main()
